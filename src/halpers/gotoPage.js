import eventBus from '@/eventBus';

export default function goToPage(pageName, pageParams) {
  eventBus.$emit('gotoPage', pageName, pageParams);
}
