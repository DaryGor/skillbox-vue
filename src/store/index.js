import Vue from 'vue';
import Vuex from 'vuex';
import products from '@/data/products';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    cartProducts: [{
      productId: 1,
      amount: 2,
    }],
  },
  mutations: {
    addProductToCart(state, { productId, amount }) {
      const elem = state.cartProducts.find((item) => item.productId === productId);

      if (elem) {
        elem.amount += amount;
      } else {
        state.cartProducts.push({
          productId,
          amount,
        });
      }
    },
    updateCardProductAmount(state, { productId, amount }) {
      const elem = state.cartProducts.find((item) => item.productId === productId);

      if (elem) {
        elem.amount = amount;
      }
    },
    deleteCartProduct(state, productId) {
      state.cartProducts = state.cartProducts.filter((item) => item.productId !== productId);
    },
    addAmount(state, productId) {
      const elem = state.cartProducts.find((item) => item.productId === productId);

      if (elem) {
        elem.amount += 1;
      }
    },
    deductAmount(state, productId) {
      const elem = state.cartProducts.find((item) => item.productId === productId);

      if (elem) {
        if (elem.amount > 0) {
          elem.amount -= 1;
        } else {
          elem.amount = 0;
        }
      }
    },
  },
  getters: {
    cartDetalProducts(state) {
      return state.cartProducts.map((item) => (
        {
          ...item,
          product: products.find((p) => p.id === item.productId),
        }
      ));
    },
    cartTotalPrice(state, getters) {
      return getters.cartDetalProducts.reduce((acc, item) => (
        (item.product.price * item.amount) + acc
      ), 0);
    },
    cartTotalProducts(state) {
      return state.cartProducts.length;
    },
  },
});
