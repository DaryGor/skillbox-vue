export default [
  {
    id: 1,
    categoryId: 4,
    title: 'Часы - будильник "Брось монетку"',
    price: 1390,
    img: 'img/money',
    colors: ['#FFF'],
  },
  {
    id: 2,
    categoryId: 4,
    title: 'Радиоуправляемая собака-робот Smart Robot Dog',
    price: 2290,
    img: 'img/dog',
    colors: ['#FFF'],
  },
  {
    id: 3,
    categoryId: 1,
    title: 'Ультразвуковая зубная щётка Playbrush Smart Sonic',
    price: 5660,
    img: 'img/toothbrush',
    colors: ['#FFF'],
  },
  {
    id: 4,
    categoryId: 2,
    title: 'Смартфон Xiaomi Mi Mix 3 6/128GB',
    price: 21790,
    img: 'img/phone-1',
    colors: ['#FFBE15'],
  },
  {
    id: 5,
    categoryId: 3,
    title: 'Электроскейт Razor Cruiser',
    price: 24690,
    img: 'img/board',
    colors: ['#000'],
  },
  {
    id: 6,
    categoryId: 2,
    title: 'Смартфон Xiaomi Mi A3 4/64GB Android One',
    price: 14960,
    img: 'img/phone-2',
    colors: ['#000'],
  },
  {
    id: 7,
    categoryId: 2,
    title: 'Смартфон Xiaomi Redmi 6/128GB',
    price: 8960,
    img: 'img/phone-3',
    colors: ['#FFBE15'],
  },
  {
    id: 8,
    categoryId: 3,
    title: 'Электрический дрифт-карт Razor Crazy Cart',
    price: 39900,
    img: 'img/bicycle',
    colors: ['#8BE000', '#000'],
  },
  {
    id: 9,
    categoryId: 3,
    title: 'Гироскутер Razor Hovertrax 2.0',
    price: 34900,
    img: 'img/wheels',
    colors: ['#000'],
  },
  {
    id: 10,
    categoryId: 3,
    title: 'Детский трюковой самокат Razor Grom',
    price: 4990,
    img: 'img/bicycle',
    colors: ['#8BE000', '#000'],
  },
  {
    id: 11,
    categoryId: 3,
    title: 'Электрический дрифт-карт Razor Crazy Cart',
    price: 39900,
    img: 'img/scooter',
    colors: ['#000'],
  },
  {
    id: 12,
    categoryId: 3,
    title: 'Роллерсёрф Razor RipStik Air Pro',
    price: 6690,
    img: 'img/ripstick',
    colors: ['#73B6EA', '#000'],
  },
  {
    id: 13,
    categoryId: 5,
    title: 'Наушники AirPods с беспроводным зарядным футляром',
    price: 16560,
    img: 'img/airpods',
    colors: ['#FFF'],
  },
  {
    id: 14,
    categoryId: 5,
    title: 'Наушники Sony',
    price: 30690,
    img: 'img/headphones',
    colors: ['#000'],
  },
];
